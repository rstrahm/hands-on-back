﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    
    public class ValuesController : ControllerBase
    {
        private readonly handsonContext _context;

        public ValuesController(handsonContext context)
        {
            _context = context;
        }

        // GET api/values
        [HttpGet("")]
        public ActionResult<List<Competidor>> GetAll()
        {
            return _context.Competidor.ToList();
        }

        [HttpGet("Marcas")]
        public ActionResult<List<Marcas>> GetAllMarcas()
        {
            return _context.Marcas.ToList();
        }

        [HttpGet("Zonas")]
        public ActionResult<List<ZonasDePrecio>> GetAllZonas()
        {
            return _context.ZonasDePrecio.ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<Competidor> GetById(int id)
        {
            var item = _context.Competidor.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        // POST api/values
        [HttpPost]
        public ActionResult Create(Competidor item)
        {
            _context.Competidor.Add(item);
            _context.SaveChanges();

            return CreatedAtRoute("GetCompetidor", new { id = item.Id }, item);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public ActionResult Update(int id, Competidor item)
        {
            var competidor = _context.Competidor.Find(id);
            if (competidor == null) 
            {
                return NotFound();
            }

            competidor.Codigo = item.Codigo;
            competidor.Nombre = item.Nombre;
            competidor.Calle = item.Calle;
            competidor.Latitud = item.Latitud;
            competidor.Longitud = item.Longitud;
            competidor.Marcador = item.Marcador;
            competidor.Observar = item.Observar;
            competidor.MarcaId = item.MarcaId;
            competidor.ZonaDePrecioId = item.ZonaDePrecioId;

            _context.Competidor.Update(competidor);
            _context.SaveChanges();
            return NoContent();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var competidor = _context.Competidor.Find(id);
            if (competidor == null)
            {
                return NotFound();
            }

            _context.Competidor.Remove(competidor);
            _context.SaveChanges();
            return NoContent();
        }
    }
}
