﻿using Microsoft.EntityFrameworkCore;

namespace WebApi.Models
{
    public partial class handsonContext : DbContext
    {
        public handsonContext(DbContextOptions<handsonContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Competidor> Competidor { get; set; }
        public virtual DbSet<Marcas> Marcas { get; set; }
        public virtual DbSet<ZonasDePrecio> ZonasDePrecio { get; set; }
    }
}
