﻿using System;
using System.Collections.Generic;

namespace WebApi.Models
{
    public partial class Competidor
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string Calle { get; set; }
        public long? Latitud { get; set; }
        public long? Longitud { get; set; }
        public bool? Marcador { get; set; }
        public bool? Observar { get; set; }
        public int? MarcaId { get; set; }
        public int? ZonaDePrecioId { get; set; }
    }
}
