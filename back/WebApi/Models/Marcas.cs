﻿using System;
using System.Collections.Generic;

namespace WebApi.Models
{
    public partial class Marcas
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
    }
}
